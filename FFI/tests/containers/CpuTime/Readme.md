## CPU time availability test

This test set helps investigate the reduction of available CPU time to one container while another one is putting different types of load onto the system.

### Interference tests

1. brk(), stack overflow and calloc() interference created using stress-ng brk, stack and bigheap stressors
1. mmap() and cpu interference created using stress-ng cpu and vm stressor
1. mmap() interference created using stress-ng tmpfs stressor
1. Interference created using basic forkbomb :(){ :|:& };:

### Mitigation tests

1. Interference mitigated using core pinning
1. Interference mitigated using `podman run --cpus`
1. Interference mitigated using `podman run --cpu-shares`
1. Interference mitigated using cgroupsv2 cpu.max
1. Interference mitigated using cgroupsv2 cpu.weight
1. Interference mitigated using cgroupsv2 cpu.weight.nice
1. Interference mitigated for forkbomb using `podman run --cpus`
