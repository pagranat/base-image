#!/bin/bash

set -euo pipefail

source /etc/os-release

if [[ "${PRETTY_NAME}" != "${DISTRO_NAME}" ]]; then
  echo "Error: the expected DISTRO_NAME was '${DISTRO_NAME}', but it was '${PRETTY_NAME}'"
  exit 1
fi
