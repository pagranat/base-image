import subprocess
import re

sysd_result = subprocess.run('systemd-analyze', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
sysd_result = sysd_result.stdout.decode('utf-8')

kernel_boottime = re.findall('(\d*\.?\d+[m]?[s])\s\(kernel\)', str(sysd_result))
initrd_boottime = re.findall('(\d*\.?\d+[m]?[s])\s\(initrd\)', str(sysd_result))
userspace_boottime = re.findall('(\d*\.?\d+[m]?[s])\s\(userspace\)', str(sysd_result))
total_boottime = re.findall('\=\s(\d*\.?\d+[m]?[s])\s', str(sysd_result))
defaultTgt_boottime = re.findall('(\d*\.?\d+[m]?[s])\sin\suserspace', str(sysd_result))

defaultTargetName = subprocess.run('/usr/bin/systemctl get-default', shell=True, stdout=subprocess.PIPE)
defaultTargetName = re.findall('\w+\.\w+', str(defaultTargetName))

print(f'Kernel boot time - {kernel_boottime[0]}')
print(f'initrd boot time - {initrd_boottime[0]}')
print(f'Userspace boot time - {userspace_boottime[0]}')
print(f'Total boot time - {total_boottime[0]}')
print(f'Default target {defaultTargetName[0]} boot time - {defaultTgt_boottime[0]}')

log = ''

journal = subprocess.run('/usr/bin/journalctl', shell=True, stdout=subprocess.PIPE)
journal = str(journal)
for line in journal.split('\\n'):
    if 'STARTUP TIMING' in line or 'first frame' in line:
        line = line.rstrip(' #') + '\n'
        log += line
if len(log) > 1:
    print(log)
else:
    raise Exception('FAIL - Neptune Startup not found')
